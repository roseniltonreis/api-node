'use strict'

import { sequelizeConn } from '../config/system.config'

module.exports = (sequelize, DataTypes) => {

	const User =  sequelizeConn.define('usuario', {
		email: {
			type: DataTypes.STRING,
			unique: true,
			validate:{
				isEmail: "Email inválido"
			}
		},
		telefone: DataTypes.STRING,
		senha: {
			type: DataTypes.STRING,
			validate:{
				function(val) {
					console.log(val);
	                if (val !== "mustbethis") throw new Error("Custom validation failed");
	            }
			}
		},
		permissao: DataTypes.STRING,
		deletado: DataTypes.STRING,
	},{
    classMethods: {
      associate: (models) => {
        // associations can be defined here
      }
    }
  })

	return User;
};
