
import { sequelizeConn } from '../config/system.config.js'

// Carrega todos os models que estiverem na pasta 'modules/models'
var models = [
  'User'
]

models.forEach(function(model) {
  module.exports[model] = sequelizeConn.import(__dirname + '/' + model);
});

// export connection
module.exports.sequelizeConn = sequelizeConn