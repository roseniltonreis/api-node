'use strict'

/*
* Rotas - Arquivo que define os caminhos das rotas da API
*/

//dependencies
import express from 'express'
import secure from '../config/secure.route'

//controllers
import AuthController from '../modules/auth/auth.controller'
import UsuarioController from '../modules/usuario/usuario.controller'
// import UserSequelizeController from '../modules/user.sequelize/user.sequelize.controller'

const router = express.Router();
//exemplo rota segura
//router.route('/rota').get([secure], controller.rota);

//Instancia os controllers
const auth = new AuthController();
const usuarioController = new UsuarioController();

// ************************************
// ********** Rotas do Modelo *********
// ************************************
router.route('/usuario')
	.get(usuarioController.listAll)
	.post(usuarioController.save);

router.route('/usuario/:id')
	.get(usuarioController.listById)
	.put(usuarioController.update)
	.delete(usuarioController.delete);

// ************************************
// ******* Rota de Autenticação ******
// ************************************
router.route('/authenticate')
	.post(auth.authenticate);
  

module.exports = router;