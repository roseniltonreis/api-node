'use strict'

/*
* Classe responsável por iniciar o server e chamar as rotas
* Deve ser previamente configurada no arquivo /config/system.config.js
* boas práticas node: https://github.com/airbnb/javascript
*/

import express from 'express'
import bodyParser from 'body-parser'
import getIP from'ipware'

//export app to other classes
const app = module.exports = express();

const allowCors = (req, res, next)=>{
	var data = new Date();
	var clientIp = getIP().get_ip(req).clientIp
	console.log(`${req.hostname}${req.originalUrl} | ${req.method} | clientIP(${clientIp}) - ${data}`)

	res.header('Access-Controll-Allow-Origin', '*');
	res.header('Access-Controll-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Controll-Allow-Headers', 'Content-Type');
	res.header('Access-Controll-Allow-Credentials', 'true');
	next();
}

app.use(allowCors);

app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(bodyParser.json());

const port = process.env.PORT || 4500;

app.listen(port, ()=> {
	console.log('Running on port ' + port);
});

