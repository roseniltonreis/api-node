'use strict'

/*
* Classe responsável por verificar se o token do usuário ainda é válido
* documentação: https://github.com/auth0/node-jsonwebtoken
*/

import jwt from 'jsonwebtoken'
import { HTTP, secret } from './system.config'

module.exports = (req, res, next)=>{

  const token = req.headers['auth-token'];

  if(token){
    jwt.verify(token, secret, (err, decoded)=>{
      if(err){
        return res.status(HTTP.OK).json({
          success: false,
          error: 'Token inválido!'
        })
      }else{
        req.decoded = decoded
        next();
      }
    })   
  }else{
    return res.status(HTTP.FORBIDDEN).send({
      success: false,
      error: 'Nenhum token encontrado'
    })
  }
}