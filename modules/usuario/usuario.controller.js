'use strict'

import { HTTP, conn } from '../../config/system.config'
import models from '../../models'

/*
* Classe responsável pelo CRUD de usuários
*/

class UsuarioController {

	/**
	 * @api {get} /usuario/
	 * @apiDescription Retorna os usuários
	 * @apiGroup Users
	 * @apiName listAll
	 *
	 */
	listAll(req, res){

		models.User.findAll().then((usuario)=> {
			return res.status(HTTP.OK).json({data: usuario});
		}, (error)=>{
			return res.status(HTTP.BAD_REQUEST).json({message: 'Erro ao listar: ' + error})
		});	

	};

	/**
	 * @api {get} /usuario/:id
	 * @apiDescription Retorna usuário pelo ID
	 * @apiParam {id} ID do usuário
	 * @apiGroup Users
	 * @apiName listById
	 *
	 */
	listById(req, res){

		const id = req.query.id;

		models.User.findById(id).then( (usuario) => {
			return res.status(HTTP.OK).json({message: `ok`, data: usuario});
		}, (error)=>{
			return res.status(HTTP.BAD_REQUEST).json({message: 'Erro ao listar: ' + error})
		})

	};

	/**
	 * @api {post} /usuario/
	 * @apiDescription Salva novo usuário
	 * @apiGroup Users
	 * @apiName save
	 *
	 */
	save(req, res) {

		const { email, telefone, senha } = req.body;

		models.User.create({email: email, telefone: telefone, senha: senha}).then((usuario)=>{
			return res.status(HTTP.OK).json({message: 'Registro cadastrado com sucesso!', data: usuario});
		}, (error)=>{
			return res.status(HTTP.NOT_FOUND).json({message: error});
		})
	};

	/**
	 * @api {put} /usuario/:id
	 * @apiDescription Altera dados do usuário
	 * @apiParam {id} ID do usuário	 
	 * @apiGroup Users
	 * @apiName update
	*/
	update(req, res) {

		models.User.find({ where: { id: req.body.id } })
			.on('success', (usuario) => {
			// Check if record exists in db
			if (usuario) {
				usuario.updateAttributes({
					//campos a serem atualizados
					campo: 'valor'
				})
				.success(() => {
					return res.status(HTTP.OK).json({message: 'Registro alterado com sucesso!', data: usuario});
				})
			}
		}, (error) => {
			return res.status(HTTP.NOT_FOUND).json({message: `Erro ao alterar o Registro: ${error}`});
		})
	};

	/**
	 * @api {delete} /usuario/
	 * @apiDescription Deleta usuário
	 * @apiGroup Users
	 * @apiName delete
	*/
	delete(req, res) {

		models.User.find(req.params.id).on('success', (usuario) => {
		  usuario.destroy().on('success', (usuario) => {
			if (usuario && usuario.deletedAt) {
			  return res.status(HTTP.OK).json({message: 'Registro deletado com sucesso!'});
			}
		  })
		}, (error) => {
			return res.status(HTTP.NOT_FOUND).json({message: 'Erro ao deletar registro:', err});
		})

	};
}

module.exports = UsuarioController