'use strict'

import jwt from 'jsonwebtoken'
import { HTTP, secret } from '../../config/system.config'
import models from '../../models'

/*
* Classe responsável pela autenticação do usuário no sistema
* Valida se o usuário já está cadastrado e gera um token, caso ele esteja
*/

class AuthController {

    /**
    * @api {post} /authenticate
    * @apiDescription Faz a autenticação do usuário e retorna o token, caso ele seja autenticado
    * @apiGroup Authentication
    * @ apiParam { username } Nome de usuário (email/usuario)
    * @ apiParam { password } Senha
    * @apiName authenticate
    */
    authenticate(req, res){

        const usuarioModel = new UsuarioModel();

        const email = req.body.email || ''
        const senha = req.body.senha || ''

        const email_validator = /\S+@\S+\.\S+/.test(req.body.email);

        if(!email_validator){
          return res.status(HTTP.FORBIDDEN).send({error: "Digite um email válido.", success: false})
        }

        models.User.find({ where: { email: email, senha: senha } })
        .on('success', (usuario) => {

            const obj_user = {
                id: usuario.id,
                email: usuario.email,
                telefone: usuario.telefone
            }
            const token = jwt.sign(obj_user, secret);

            return res.status(HTTP.OK).json({
                status: true,
                message: 'Token gerado com sucesso',
                token: token
            })

            //token com tempo de expiração
            // const token = jwt.sign(user, superSecret, {
            //   expiresInMinutes: 1440 //expira em 24 horas
            // })

        }, (error) => {
            return res.status(HTTP.BAD_REQUEST).json({message: `Erro ao gerar token: ${error}`});
        })


    }
}

module.exports = AuthController