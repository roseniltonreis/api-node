'use strict'

import express from 'express'
 
//Base do Setup da API:
import app from './config/app.config'
import routes from './routes'

app.use('/api', routes);