# API NODE REST + DB MYSQL + JWT + ES6 + APIDOC #

para rodar o projeto, fa�a o clone, configure o banco de dados no arquivo 'api-node/config/system.config.js',
execute o 'npm install' e depois 'npm start'

> git clone https://roseniltonreis@bitbucket.org/roseniltonreis/api-node.git
> cd api-node
> npm install
> npm start

# ATUALIZAR INFORMA��ES DA DOCUMENTA��O #
> apidoc -i modules -o doc/ -t node_modules/apidoc/template/

# ESTRUTURA #
 
|
|-- api-node/
|   |-- common
|		|-- send.mail.js (enviar emails)
|-- config/
|   |-- app.config.js (inicia server e chama as rotas)
|   |-- secure.route.js (rotas seguras)
|   |-- system.config.js (configura��es gerais do sistema)
|
|-- doc/ (documenta��o da API)
|
|-- models
|	|--index.js (auto carrega os models da pasta)
|	|--User.js (model exemplo)
|
|-- modules/
|   |-- auth (modulo respons�vel pela autentica��o do usu�rio)
|   	|-- auth.controller.js
|   |-- usuario
|   	|-- usuario.controller.js
|   	|-- usuario.model.js
|
|-- routes/
|   |-- index.js (responsavel por gerencias as rotas da API)
|
|-- server.js (respons�vel por iniciar a API)


# COMPONENTES EXTRAS #
apidoc js, para gerar a documenta��o
http://apidocjs.com/

JWT
https://jwt.io/



