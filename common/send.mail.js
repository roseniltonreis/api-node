import nodemailer from 'nodemailer'
import { emailConfig } from '../config/system.config'

/*
* Classe responsável por enviar emails
* Deve ser previamente configurada no arquivo /config/system.config.js
* documentação: https://nodemailer.com/smtp/
*/

class EnviarEmail{

	send(email, assunto, mensagem){

		const transporter = nodemailer.createTransport({
			service: emailConfig.serviceMail,
			auth: emailConfig.auth
		})

		const mailOptions = {
			from: emailConfig.userMailFrom,
			to: email,
			subject: assunto,
			text: mensagem
		}

		return new Promise((resolve, rejected)=>{
			transporter.sendMail(mailOptions, (error, info)=>{
				if(error){
					console.log("Erro ao enviar email: ", error);
					return rejected({message: "Erro ao enviar email"})
				}else{
					return resolve({message: `email enviado: com sucesso para '${mailOptions.to}'`});
				}
			})
		})
		
	}

}

module.exports = EnviarEmail;